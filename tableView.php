<?php  
	require_once('settings.php'); 
    require_once('config.php'); 

	if(!isUserLogin()){
		Leave(SITE_URL);
	}

	if(isset($_GET['logout'])){
		doLogout();
		Leave(SITE_URL);
	}
	$name = '';
	$slug = '';
	$sql = '';
	$limit = '';
	$where = '';
	$order = '';
	$group = '';
	$selectedTables = array();
	$joinedTables = array();
	//tables we dont want to show on tableView add/edit pages
	$excludeTables = array(DB_PREFIX."users", DB_PREFIX."options", DB_PREFIX."table_views");
	$allTables = $db->query('SELECT table_name FROM information_schema.tables where table_schema="'.DB_NAME.'" ');
	
	//add or insert table view (save data)
	$insertMsg = "";
	$actionName = 'insert_table_view';
	$buttonName = 'Insert Table View';
	if(isset($_POST['insert_table_view'])){		
		extract($_POST);
		$insert = saveTableView($db, 'insert', $_POST);
		$insertMsg = $insert['message'];
		$insertStatus = $insert['status'];
	}

	//edit or update table view (save data)
	if(isset($_POST['update_table_view'])){	
		extract($_POST);
		var_dump($_POST); 
		$insert = saveTableView($db, 'update', $_POST);
		$insertMsg = $insert['message'];
		$insertStatus = $insert['status'];
	}
	
	//edit table view page
	if (isset($_GET['edit']) && $_GET['edit'] != '') {
		$viewSlug = $_GET['edit'];
		$view = $db->row("SELECT * FROM `".DB_PREFIX."table_views` WHERE view_slug = :view_slug", array("view_slug"=>$viewSlug));		
		$objectView = unserialize($view['view_object']);
		//var_dump($objectView);
		$name = $objectView['name'];
		$slug = $objectView['slug'];
		if(isset($objectView['tables']['selected'])){
			$selectedTables = $objectView['tables']['selected'];
		}		
		if(isset($objectView['tables']['joined'])){
			$joinedTables = $objectView['tables']['joined'];
		}
		$actionName = 'update_table_view';
		$buttonName = 'Update Table View';
	}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>DBQuest App Dashboard</title>
		<link rel="stylesheet" type="text/css" href="assets/css/styles.css">

	</head>
	<body>
		<div class="container">
			<ul class="dashboard-nav">				
				<li><a href="<?php echo SITE_URL; ?>/dashboard.php">Dashboard</a></li>
				<li><a href="<?php echo SITE_URL; ?>/tableViews.php">All Table Views</a></li>

				<li class="pull-right"><a class="logout" href="<?php echo thisUrl(); ?>/?logout">Log Out</a></li>
			</ul>


			<div class="createTableView">
				<?php if($insertMsg!=""){ ?>
					<div class="msgInfoBlock <?php echo $insertStatus; ?>"><?php echo $insertMsg; ?></div>
				<?php } ?>

				<h2>Create New Table View</h2>
				
				<form action="" method="POST">
					
					<div class="input-group name">
						<label>Name</label>
						<input type="text" name="name" placeholder="Table View Name" value="<?php echo $name; ?>" required='required'/>
					</div>
					<div class="input-group slug">
						<label>Slug</label>
						<input type="text" name="slug" placeholder="Table View Slug" value="<?php echo $slug; ?>" required='required'/>
					</div>
					<div class="input-group saveBtn pull-right">
						<input name="<?php echo $actionName ?>" type="submit" value="<?php echo $buttonName ?>" />
					</div>

					<div class="line"></div>
					<div class="alldbTable">
						<div id="select-tables" class="selectFromTables">
						<h3>Select Table</h3>
						<?php
						foreach ($allTables as $key => $value) {
							if(!in_array($value['table_name'], $excludeTables)){
								$tableName = $value['table_name'];
								if(array_key_exists($tableName, $selectedTables)){
									$checked = 'checked="checked"';
									$activeClass = ' active';
								} else {
									$checked = '';
									$activeClass = '';
								}
								$tableNameAttr = "tables[selected][$tableName]";
								echo "<div class='dbTable' id='select-".$value['table_name']."'>";
								echo "<input type='checkbox' ".$checked." class='dbTableName showColls' name='".$tableNameAttr."' data-table-name='".$value['table_name']."'/><span class='showColls ".$activeClass."'>".$value['table_name']."</span>";
								$columns = $db->query('SHOW COLUMNS FROM '.$tableName);
								echo "<div class='popup'><div class='close'>x</div>";
								echo "<div class='dbTableCollumns'>";
								echo "<span class='selectAllColumns'>Check All</span>";
								echo "<span class='deselectAllColumns pull-right'>Uncheck All</span>";
								foreach ($columns as $k => $v) {
									$fieldName = $v['Field'];
									$fieldNameAttr = "tables[selected][$tableName][$fieldName]";
									$callChecked = '';
									if(isset($selectedTables[$tableName])){
										if(array_key_exists($fieldName, $selectedTables[$tableName])){
											$callChecked = ' checked="checked"';
										}
									}
																		
									echo "<div class='dbTableColl' style='padding-bottom:10px;'>&nbsp; &nbsp; <input name='".$fieldNameAttr."' type='checkbox' ".$callChecked." class='tableColumn' data-column-name='".$v['Field']."'><input type='text'  placeholder='".$v['Field']."' value='' ></div>";
								}
								echo '</div></div></div>';
							}							
						}
						 ?>
						</div> 

						<div id="join-tables" class="">
						<h3>Join Table</h3>
						<?php													
						foreach ($allTables as $key => $value) {
							if(!in_array($value['table_name'], $excludeTables)){
								$tableName = $value['table_name'];

								if(array_key_exists($tableName, $joinedTables)){
									$checked = 'checked="checked"';
									$activeClass = ' active';
									$joinOnVal = $joinedTables[$tableName]['joinOn'];
									$tableJoinTypeNameAttr = "tables[joined][".$tableName."][joinType]";
									$tableJoinOnNameAttr = "tables[joined][".$tableName."][joinOn]";
									$selectedDropdown = '<select name="'.$tableJoinTypeNameAttr.'" class="pull-right"><option value="JOIN">JOIN</option><option selected="selected" value="LEFT JOIN">LEFT JOIN</option><option value="RIGHT JOIN">RIGHT JOIN</option><option value="OUTER JOIN">OUTER JOIN</option></select>';
									$joinOnHtml ='<div class="joinOn"><label>ON</label><textarea name="'.$tableJoinOnNameAttr.'" class="joinOnVal" placeholder="table1.field1=table2.field2">'.$joinOnVal.'</textarea></div>';
								} else {
									$checked = '';
									$activeClass = '';
									$selectedDropdown = '';
									$joinOnHtml = '';
									$joinOnVal = '';
								}
								$tableNameAttr = "tables[joined][$tableName]";
								echo "<div class='dbTable' id='join-".$value['table_name']."'> ";
								echo "<input type='checkbox' ".$checked." class='dbTableName showColls' name='".$tableNameAttr."' data-table-name='".$value['table_name']."'/><span class='showColls ".$activeClass."'>".$value['table_name']."</span>";
								echo '<span class="joinTypesDropdown">'.$selectedDropdown.'</span>';
								echo '<div class="joinOnHtml">'.$joinOnHtml.'</div>';
								$columns = $db->query('SHOW COLUMNS FROM '.$tableName);
								echo "<div class='popup'><div class='close'>x</div>";
								echo "<span class='selectAllColumns'>Select All</span>";
								echo "<span class='deselectAllColumns pull-right'>Uncheck All</span>";
								echo "<div class='dbTableCollumns'>";
								foreach ($columns as $k => $v) {
									$fieldName = $v['Field'];
									$fieldNameAttr = "tables[joined][$tableName][$fieldName]";
									$callChecked = '';
									if(isset($joinedTables[$tableName])){
										if(array_key_exists($fieldName, $joinedTables[$tableName])){
											$callChecked = ' checked="checked"';
										}
									}
									
									echo "<div class='dbTableColl' style='padding-bottom:10px;'>&nbsp; &nbsp; <input name='".$fieldNameAttr."' type='checkbox' ".$callChecked." class='tableColumn' data-column-name='".$v['Field']."'><input type='text' placeholder='".$v['Field']."' value=''></div>";
								}
								echo '</div></div></div>';
							}
						}
						 ?>
						</div> 

						<div id="conditions" class="">
							<h3>Conditions</h3>	
							<p class="condition">
								<label>WHERE</label>
								<textarea name="condition[where]" id="condition-where" placeholder="t1.id=1 AND t1.name LIKE %joe%" value="<?php echo $where; ?>"></textarea>
							</p>						
							<p class="condition">
								<label>GROUP BY</label>
								<textarea name="condition[groupBy]" id="condition-group" placeholder="column1, column2" value="<?php echo $group; ?>"></textarea>
							</p>
							<p class="condition">
								<label>ORDER BY</label>
								<textarea name="condition[orderBy]" id="condition-order" placeholder="FirstName DESC, YearOfBirth ASC" value="<?php echo $order; ?>"></textarea>
							</p>
							<p class="condition">
								<label>LIMIT </label>
								<input id="condition-limit" type="number" min="1" name="condition[limit]" placeholder="20"  value="<?php echo $limit; ?>" />
							</p>
						</div>
					</div>

					<div id="generate-query" class="text-center">
						<div id="alertMessage"></div>
						<p>
							<span class="showQueryBtn">Show Query Block</span>
						</p>
						<div class="input-group">
							<textarea id="show-query" disabled="disabled" value="<?php echo $sqlView; ?>"></textarea>
						</div>
					</div>
					<div class="line"></div>
				</form>


			</div>
			
		</div>

		<div class="footer">
			<a style="color: #000;" href="#">DBQuest::FMF</a>
		</div>

		<script src="assets/libs/jquery-3.3.1.min.js"></script>
		<script src="assets/js/scripts.js"></script>
		

	</body>
</html>