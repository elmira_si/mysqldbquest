<?php


$status = "";
$succ = "<span style=\"color:green\">Done!</span><br />";
$fail = "<span style=\"color:red\">Failed.</span><br />";
$err = false;
$msg = '';

if( isset($_POST['insall_dbquest'])){

	// Defaults
	if ($_POST['db_server'] == ""){
		$_POST['db_server'] = "localhost";
	}
	if ($_POST['db_password'] == ""){
		$_POST['db_password'] = "";
	}
	if ($_POST['db_prefix'] == ""){
		$_POST['db_prefix'] = "";
	}

	// Validation
	foreach ($_POST as $p)
	{
		//if ($p == "") $err = "You left a field blank. Please press the back button and try again.";
	}

	//Creating config file from installer form
	$filename = '../config.php';
	if (!file_exists($filename)) {
		if (!copy('../config-sample.php', '../config.php')) {
		    echo "failed to copy config-sample.php";
		    $msg .= "Failed to copy config-sample.php";
			$err = "Failed to create config.php file.";
		}
	} else {
	    $msg .= "Unable to create ".$filename;
		$err = "File config.php is already exists.";
	}

	// Changeing Placeholders to given form values
	if (!$err){
		$f = file_get_contents("../config.php");		
		$f = str_replace("DB_SERVER_PLACEHOLDER", $_POST['db_server'], $f);
		$f = str_replace("DB_USER_PLACEHOLDER", $_POST['db_user'], $f);
		$f = str_replace("DB_PASSWORD_PLACEHOLDER", $_POST['db_password'], $f);
		$f = str_replace("DB_NAME_PLACEHOLDER", $_POST['db_name'], $f);
		$f = str_replace("DB_PREFIX_PLACEHOLDER", $_POST['db_prefix'], $f);
		
		if (!file_put_contents("../config.php", $f)){
			$msg .= "Writing config file... ".$fail;
			$err = "Unable to open config.php for writing. Try changing permissions.";
		}else{
			$msg .= "Writing config file... ".$succ;
			require('../config.php');
		}
	}


	// Default SQL Tables
	if (!$err)
	{
		if ($mysql_err)		{
			$msg .= "Checking database connection... ".$fail;
			$err = $mysql_err;
		}else{
			$msg .= "Checking database connection... ".$succ;
			$dbPrefix = $_POST['db_prefix'];

			//create options table
			$sql = "CREATE TABLE `".$dbPrefix."options` (
					  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `option_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
					  `option_value` longtext COLLATE utf8_unicode_ci NOT NULL,
					  `autoload` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
					  PRIMARY KEY (`option_id`),
					  UNIQUE KEY `option_name` (`option_name`)
					) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
			$db->query($sql);			

			//create table_views table
			$sql = "CREATE TABLE `". $dbPrefix."table_views` (
					  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `view_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
					  `view_slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',					 
					  `view_object` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
					  `view_sql` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',					 
					  PRIMARY KEY (`ID`)
					) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
			$db->query($sql);

			//create users table
			$sql = "CREATE TABLE `". $dbPrefix."users` (
					  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					  `user_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
					  `user_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',					 
					  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
					  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  `user_activation_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
					  `user_status` int(11) NOT NULL DEFAULT '1',
					  `user_role` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'viewer', 
					  PRIMARY KEY (`ID`)
					) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
			$db->query($sql);

			//Insert initial user data to users table
			$adminUsername = $_POST['admin_username'];
			$adminPassword = $_POST['admin_password'];
			$sql = sprintf(
					"INSERT INTO `". $dbPrefix."users` 
					(user_login, user_pass, user_email, user_role) 
					VALUES ('%s', '%s', '', 'super')",
					$adminUsername,
					hash('whirlpool', $adminPassword)
			);
			$db->query($sql);
			
			
			$msg .= "Running SQL queries... ".$succ;
		}
	}
}else{
	$url = "http://".$_SERVER["HTTP_HOST"]."/install/";
	header("Location: $url");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
    </title>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
    rel="stylesheet">
  </head>
  
  <body>
    <div class="container">
      <div class="jumbotron">
        <?php 
		if (!$err)
		{ ?>
		<h1>Installation complete.</h1>
		<!-- <p><strong>PLEASE DELETE THE ENTIRE /install/ FOLDER FROM THE SERVER.</strong></p> -->
		<p>Click <a href="<?php echo SITE_URL ?>">here</a> to continue.</p>
		<?php 
		}
		else
		{ ?>
		<h1>There was a problem during installation.</h1>
		<p><?php echo $err ?></p>
		<?php
		} ?>
      </div>
	  <p><?php echo $msg ?></p>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
  </body>

</html>