<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
    </title>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
    rel="stylesheet">
  </head>
  
  <body>
	<form action="do.php" method="post">
    <div class="container">
      
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
			<h2>Install <span style="color: #00d6e2;">DBQuest</span></h2>
			<p>Fill out the form bellow to get started with your install.</p>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Database Information
              </h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label>
                  MySQL Server <span style="color:#f00;">*</span>
                </label>
                <input type="text" class="form-control" name="db_server" placeholder="127.0.0.1" required="required" />
              </div>
              <div class="form-group">
                <label>
                  MySQL Username <span style="color:#f00;">*</span>
                </label>
                <input type="text" class="form-control" name="db_user" placeholder="root" required="required" />
              </div>
              <div class="form-group">
                <label>
                  MySQL Password
                </label>
                <input type="password" class="form-control" name="db_password" />
              </div>
			  <div class="form-group">
                <label>
                  Database Prefix
                </label>
                <input type="text" class="form-control" name="db_prefix" placeholder="dbq_" >
              </div>
              <div class="form-group">
                <label>
                  MySQL Database Name <span style="color:#f00;">*</span>
                </label>
                <input type="text" class="form-control" name="db_name" placeholder="dbquest" required="required" />
              </div>
			  
            </div>
          </div>
		  
		  <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Admin Account
              </h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label>
                  Username <span style="color:#f00;">*</span>
                </label>
                <input type="text" name="admin_username" class="form-control" placeholder="admin" required="required" />
              </div>
			  <div class="form-group">
                <label>
                  Password <span style="color:#f00;">*</span>
                </label>
                <input type="password" name="admin_password" class="form-control" placeholder="admin" required="required" /> 
              </div>
            </div>
          </div>
          
			<p>
				<input type="submit" class="btn btn-primary btn-lg" name="insall_dbquest" value="Install" />
			</p>
		  
        </div>
       
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<form action="do.php" method="post">
  </body>

</html>