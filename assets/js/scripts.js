$( document ).ready(function() {

	//show popup with table columns when table checkbox checked
	$(".dbTableName").change(function() {
	    if(this.checked) {
	        //if table name checked
	        $('.popup').hide();
			$(this).parent().find('.showColls').addClass('active');
			$(this).parent().find('.popup').show();			
	    }else{
	    	$(this).parent().find('.showColls').removeClass('active');
	    	$(this).parent().find('.popup').hide();
	    	$(this).parent().find('.tableColumn').prop('checked', false); // Unchecks it
	    }
	    var sqlContainer = $('#show-query');
		generateSqlQuery(sqlContainer);
	});
	$("#join-tables .dbTableName").change(function() {
		var thisTable = $(this).data('table-name');
		var tableJoinOnNameAttr = "tables[joined]["+thisTable+"][joinOn]";
		var tableJoinTypeNameAttr = "tables[joined]["+thisTable+"][joinType]";

		var selectdropdown = '<select name="'+tableJoinTypeNameAttr+'" class="pull-right"><option value="JOIN">JOIN</option><option selected="selected" value="LEFT JOIN">LEFT JOIN</option><option value="RIGHT JOIN">RIGHT JOIN</option><option value="OUTER JOIN">OUTER JOIN</option></select>'; 
		var joinOnHtml ='<div class="joinOn"><label>ON</label><textarea name="'+tableJoinOnNameAttr+'" class="joinOnVal" placeholder="table1.field1=table2.field2"></textarea></div>';

		if(this.checked) {						
			$('#join-'+thisTable+' .joinTypesDropdown').html(selectdropdown);
			$('#join-'+thisTable+' .joinOnHtml').html(joinOnHtml);		
	    }else{
	    	console.log(thisTable);
	    	$('#join-'+thisTable+' .joinTypesDropdown').html('');
	    	$('#join-'+thisTable+' .joinOnHtml').html('');
	    }		
	});

	//open columns popup when table name clicked
	$('.dbTable').delegate('span.active', 'click', function(e){
		$('.popup').hide();
		$(this).parent().find('.popup').show();
	});
	//table columns change funcion
	$('.tableColumn').change(function() {
	    if(this.checked) {
	        //checked
			var sqlContainer = $('#show-query');
   			generateSqlQuery(sqlContainer);
	    }
	});
	// Close table fields popup
	$('.popup .close').on('click', function(e){
		$('.popup').hide();
	});
	// show/hide JOIN ON condition input
	$('.joinOnShow').on('click', function(){
		$(this).parent().find('.joinOn').toggle();
	});
	// Check and Uncheck all checkboxes 
	$('.selectAllColumns').on('click', function(e){
		$(this).parent().find('.tableColumn').prop("checked", true);
	});
	$('.deselectAllColumns').on('click', function(e){
		$(this).parent().find('.tableColumn').prop("checked", false);
	});
	
    //Show generate/Show query textarea with generated query
	$('.showQueryBtn').on('click', function(e){
		$('.popup').hide();
		if( !$(this).hasClass('active') ){			
			var messageBlock = "#generate-query #alertMessage";	
			var message = '';
			var selector = '';
			var type = '';
			//checking if any select tables checked	
			var selectTables = '#select-tables .dbTableName';
			var selectMsg = 'Plese Select Table <br/>';		
			var checkSelectTables = isChecked(selectTables, messageBlock, selectMsg); 			
			if(checkSelectTables){ //if any table selected				
				checkedTablesFields('select', selectTables); //checking Selected Table Columns
				//check if any Join tabel selected
				var joinTables = '#join-tables .dbTableName';	
				var checkJoinTables = isChecked(joinTables, '', ''); 
				if(checkJoinTables){
					checkedTablesFields('join', joinTables); //checking Join Table Columns			
				}
			}			
		}else{
			$(this).removeClass('active');
			$('#show-query').hide();
		}
	});

	

	/* 
	*
	* @type -> select or join
	*
	*/
	function checkedTablesFields(type, selector){

		var checkedTables = $(selector+":checkbox:checked");
		var collMessage = '';
		var columnSelector = '';
		var joinOnVal = '';	
		
		checkedTables.each(function (key, val) {
		   	var thisTable = $(this).data('table-name');
		   	if(type=='select'){
				columnSelector = "#select-tables #select-";
			}
			if(type =='join'){
				columnSelector = "#join-tables #join-";
				joinOnVal = $(columnSelector+thisTable+' .joinOnVal').val();	
				if(joinOnVal==''){
					collMessage += 'Please add <span>JOIN ON</span> statement for <span>`'+thisTable+'`</span>  table <br/>';
				}			
			}
		   	var collSelector = columnSelector+thisTable+' .tableColumn';		  
		    var checkedTableFields = $(collSelector+":checkbox:checked");		    
		    if(checkedTableFields.length>0){
				$(this).addClass('active');
				$('#show-query').show();	

		    }else{		    	
		   		collMessage += 'Please Select at least one collumn for <span>`'+thisTable+'`</span>  table <br/>';
		    }
		    $('#generate-query #alertMessage').html(collMessage);		
		});	

		var sqlContainer = $('#show-query');
		generateSqlQuery(sqlContainer);		
	}

	//check if any checkbox checked and write message if not
	function isChecked(selector, messageBlock='', message=''){	
		messageBlock = (messageBlock=='')?'#generate-query #alertMessage':messageBlock;
		var checkboxes = $(selector+":checkbox:checked");		
		if(checkboxes.length == 0){
			if(message != '' ){ 
				$(messageBlock).html(message);  			
			}
			return false;
		}
		return true;		
	}

	
	/*
	* @container textarea selector wher quryy should generat
	* @type select or join
	*/
	function generateSqlQuery(container){
		$sql = '';
		var columns = [];
		var columnsStr = '';
		var tablesStr = '';
		var joinStr = '';
		var selectAll = false;
		var checkboxes = '';
		
		var checkboxesSelect = $("#select-tables .dbTableName:checkbox:checked");	
		var checkboxesJoin = $("#join-tables .dbTableName:checkbox:checked");	
		//checking for selected tabel checkboxex
		if(checkboxesSelect.length>0){
			checkboxesSelect.each(function (key, val) {
				var thisTable = $(this).data('table-name');
				tablesStr += '`'+thisTable+'`, ';
				var columnsChecked = $("#select-tables #select-"+thisTable+' .tableColumn:checkbox:checked');
				var columnsAll = $("#select-tables #select-"+thisTable+' .tableColumn');			
				if(columnsChecked.length == columnsAll.length){
					columns.push({'field': thisTable+'.*'});
					columnsStr += thisTable+'.*, ';
				}else{
					columnsChecked.each(function (key, val) {
						var thisCollumnName = $(this).data('column-name');
						columns.push({'field': thisTable+'.'+thisCollumnName});
						columnsStr += thisTable+'.'+thisCollumnName+', ';
					});	
				}			
			});
		}
		//checking for joined tabel checkboxex
		if(checkboxesJoin.length>0){
			checkboxesJoin.each(function (key, val) {
				var thisTable = $(this).data('table-name');
				var joinType = $('#join-'+thisTable+' select option:selected').val();
				var joinOnVal = $('#join-'+thisTable+' .joinOnVal').val();
				var collMessage = '';
				var columnsChecked = $("#join-tables #join-"+thisTable+' .tableColumn:checkbox:checked');
				var columnsAll = $("#join-tables #join-"+thisTable+' .tableColumn');

				if(columnsChecked.length>0){
					
					joinStr += joinType +" `"+thisTable+'` \n';
					if(joinOnVal != ''){
						joinStr += "ON "+joinOnVal+' \n';
					}
					
					if(columnsChecked.length == columnsAll.length){
						columns.push({'field': thisTable+'.*'});
						columnsStr += thisTable+'.*, ';
					}else{
						columnsChecked.each(function (key, val) {
							var thisCollumnName = $(this).data('column-name');
							columns.push({'field': thisTable+'.'+thisCollumnName});
							columnsStr += thisTable+'.'+thisCollumnName+', ';
						});	
					}	
				}						
			});
		}
		//condition fields
		var whereVal= $('#condition-where').val();
		var groupByVal= $('#condition-group').val();
		var orderByVal= $('#condition-order').val();
		var limitVal= $('#condition-limit').val();
		
		columnsStr = columnsStr.trim();
		columnsStr = columnsStr.substring(0, columnsStr.length-1);			
		tablesStr  = tablesStr.trim();
		tablesStr  = tablesStr.substring(0, tablesStr.length-1);

		if(columnsStr != ''){
			$sql += "SELECT "+columnsStr+' \n';
		}
		if(tablesStr != ''){
			$sql += "FROM "+tablesStr+' \n';
		}
		if(joinStr != ''){
			$sql += joinStr;
		}
		if(checkboxesSelect.length>0){
			if(whereVal != ''){
				$sql += 'WHERE '+whereVal+' \n';;
			}
			if(groupByVal != ''){
				$sql += 'GROUP BY '+groupByVal+' \n';;
			}
			if(orderByVal != ''){
				$sql += 'ORDER BY '+orderByVal+' \n';;
			}
			if(limitVal != ''){
				$sql += 'LIMIT '+limitVal+' \n';;
			}
		}

		container.val($sql);		
	}


});