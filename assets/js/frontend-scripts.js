$( document ).ready(function() {

	$('#addFilter').on('click', function(e){
		$('.modal#filterBlock').show();
	});
	$('#closeModal').on('click', function(e){
		$('.modal').hide();
	});

	var count = 1;
	$('.fieldAction').delegate(".cloneFilterFields", "click", function(event) {
		event.preventDefault();
	 	count += 1;
		
		var node = $(this).closest('.filterFieldBlock').clone().insertBefore('.filterActionBtn');
		node.attr('data-id', count);		
		node.find('.fa-plus-circle').removeClass('fa-plus-circle cloneFilterFields').addClass('fa-minus-circle removeFilterFields');
		

		node.find('.switch-input.queryAnd').attr('id', 'and-'+count);
		node.find('.switch-input.queryAnd').attr('name', 'view-'+count);
		node.find('.switch-label.queryAndLabel').attr( 'for', 'and-'+count);		

		node.find('.switch-input.queryOr').attr('id', 'or-'+count);
		node.find('.switch-input.queryOr').attr('name', 'view-'+count);
		node.find('.switch-label.queryOrLabel').attr('for', 'or-'+count);
	});

	$('body').delegate('.removeFilterFields', 'click',  function() {
	   $(this).closest('.filterFieldBlock').remove();
	});


});