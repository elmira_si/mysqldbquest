<?php  
    require_once('settings.php'); 
    require_once('config.php');

    if(!isUserLogin()){
        Leave(SITE_URL);
    }
    if(isset($_GET['logout'])){
        doLogout();
        Leave(SITE_URL);
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>DBQuest - Table <?php echo $_GET['view'];?></title>
    <!-- fontawesome -->
    <script defer src="/assets/libs/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
    <body>
        <div class="container">

            <?php 
                $getView = $db->row("SELECT * FROM `".DB_PREFIX."table_views` WHERE view_slug = :view_slug", array("view_slug"=>$_GET['view']));
                $query   = false;
                $countRecords = $db->row("SELECT COUNT(*) AS count FROM `patients");
                $itemCount = $countRecords['count'];
                $pageUrl = SITE_URL.'/table.php?view='.$getView['view_slug'].'&page=%d';
                $pageNumber = (isset($_GET['page']) && $_GET['page'] != '') ? $_GET['page'] : 1;

                $viewObject = unserialize($getView['view_object']);

                $limit = isset($viewObject["limit"]) ? $viewObject["limit"] : 10;

                $offset = ($pageNumber-1) * $limit;
                
                if($getView){
                    $tableName = $getView['view_name'];                                      						
                }                 
                if(isset($_POST['search'])){ 
                    $fileds = $_POST['fields'];
                    $conditions = $_POST['conditions'];
                    $values = $_POST['values'];
                    $clauses = $_POST['clauses'];
                    $length = count($_POST['fields']); 
                    $searchQuery = "";
                    $searchS = "";
                    $searchArr = array();
                    $conditionList = array(
                        'equal'    => '=',
                        'notequal' => '<>',
                        'greater'  => '>',
                        'less'     => '<',
                        'percent'  => '%'
                    );
                    for($i=0; $i<$length; $i++){ 
                        $clause = ($i==0) ? '' : $clauses[$i];
                        $searchQuery .= " ". $clause . " ". $fileds[$i] . " " . $conditions[$i] . " '" . $values[$i]."'";
                        
                        $searchArr[] = array('k'=>$fileds[$i], 'v'=>$values[$i], 'op'=>$conditions[$i], 'cl'=>$clause);
                        $searchS .= "k=$fileds[$i];v=$values[$i];op=$conditions[$i];cl=$clause|";
                    }
                    $sql = $getView['view_sql']." WHERE $searchQuery AND id>$offset LIMIT $limit";
                    
                    $countRecords = $db->row("SELECT COUNT(*) AS count FROM `patients` WHERE $searchQuery");
                    $itemCount = $countRecords['count'];
                    $pageUrl = SITE_URL.'/table.php?view='.$getView['view_slug'].'&q={'.urlencode($searchS).'}&page=%d';
                    $pageNumber = (isset($_GET['page']) && $_GET['page'] != '') ? $_GET['page'] : 1;
                                        
                    $query   =  $db->query($sql);
                     //var_dump(count($query));
                     
                } else{                     
                    $sql = $getView['view_sql']." WHERE id>$offset LIMIT $limit"; 
                    $query   =  $db->query($sql);
                }
            ?>
                            <?php //echo "<pre>"; print_r($sql);echo "</pre>"?>


            <ul class="breadcrambs">				
                <li><a href="<?php echo SITE_URL; ?>/dashboard.php">Dashboard</a> &#8594; </li>
                <?php if($query){ ?>
                <li><a href="<?php echo SITE_URL; ?>/dashboard.php">Table Views</a> &#8594; </li>
                <li><?php echo $tableName; ?></li> 
                <?php } else {?>
                <li><a href="<?php echo SITE_URL; ?>/dashboard.php">Table Views</a></li>
                <?php }?>
            </ul>

            <?php if($query){ ?>
                
            
                <div class="filterActions">
                    <input id="addFilter" type="button" name="" value="Add Filter">
                    <input id="resetFilter" type="button" name="" value="Reset Filter">
                </div>

                <div class="modal" id="filterBlock">
                    <form action="" method="POST" >
                        <input type="hidden" name="view" value="<?php echo $_GET['view'];?>" />
                        <div class="filterBlock">
                            <div class="headline">Custom Filter <span id="closeModal">X</span></div>
                            <div class="fieldHeading">
                                    <span class="fieldName">Field Name</span>
                                    <span class="fieldName">Condition</span>
                                    <span class="fieldName">Value</span>
                                    <span class="fieldName">Clause </span>
                                    <span class="fieldName"> </span>
                            </div>
                            <div class="filterFieldBlock" data-id='1'>
                                    <div class="field">							
                                        <select name="fields[]">
                                            <?php foreach($query[0] as $k=>$v) {?>
                                                <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="field">							
                                        <select name="conditions[]">
                                            <option value="equal">=</option>
                                            <option value="notequal">&lt;&gt;</option>
                                            <option value="greater">&gt;</option>
                                            <option value="less">&lt;</option>
                                            <option value="percent">&#37;</option>
                                        </select>
                                    </div>
                                    <div class="field">							
                                        <input type="text" name="values[]">
                                    </div>
                                    <div class="field andOrSwitch">
                                        <select name="clauses[]">
                                            <option value=""> --- </option>
                                            <option value="or">OR</option>
                                            <option value="and">AND</option>                                            
                                        </select>
<!--                                        <div class="switch switch-blue">
                                            <input type="radio" class="switch-input queryAnd" name="view-1[]" value="and" id="and" checked>
                                            <label for="and" class="switch-label switch-label-off queryAndLabel">AND</label>
                                            <input type="radio" class="switch-input queryOr" name="view-1[]" value="or" id="or">
                                            <label for="or" class="switch-label switch-label-on queryOrLabel">OR</label>
                                            <span class="switch-selection"></span>
                                        </div>						-->
                                    </div>
                                    <div class="field fieldAction">
                                        <i class="fas fa-plus-circle cloneFilterFields"></i>
                                    </div>
                            </div>
                            <div class="filterActionBtn">					
                                <input type="submit" name="search" value="Search" class="filter">
                                <input type="submit" name="all_filters" value="All Filters" class="filter all_filters">
                                <input type="submit" name="save_filter" value="Save Filter" class="filter save_filter">
                            </div>
                        </div>
                    </form>
                </div>           

                <div class="tableView">				
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <?php foreach($query[0] as $k=>$v) {?>
                                    <th><?php echo $k; ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for($i=0; $i<count($query); $i++){ ?>
                            <tr>
                                <?php foreach($query[$i] as $k=>$v) {?>
                                    <td data-table="" data-field="<?php echo $k; ?>"> <?php echo $v; ?></td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php echo pagination($itemCount, $limit, $pageNumber, $pageUrl); ?>				
                </div>	            
            <?php } else { ?>
            <h3 class="no_data">Sorry there is not data for this request.</h3>
            <?php } ?>
        </div>


        <div class="footer">
            <a style="color: #000;" href="#">DBQuest::FMF</a>
        </div>

        <script src="assets/libs/jquery-3.3.1.min.js"></script>
        <script src="assets/js/frontend-scripts.js"></script>

    </body>
</html>