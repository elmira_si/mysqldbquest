<?php 
	require_once('settings.php'); 
    require_once('config.php'); 

    $showMsg = doLogin($db);

	if(isUserLogin()){
		Leave('dashboard.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>DBQuest App</title>
		<link rel="stylesheet" type="text/css" href="assets/css/login.css">
		<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
	</head>
	<body>
		<div class="container">

			<?php if( $showMsg && $showMsg != "" ){ ?>	
				<div class="warning">
				  <strong>Warning!</strong> Username or password is not correct.
				</div>
			<?php } ?>

			<form id="signup" action="" method="post">
				<div class="header">			
					<h3>Sign In</h3>				
					<p>Fill out this form to login the system</p>				
				</div>
				
				<div class="sep"></div>			
					<div class="inputs">				
						<input type="text" placeholder="Username" name="username" autofocus required="required" />	
						<input type="password" placeholder="Password" name="password"  required="required"/>		
						<input id="submit" type="submit" value="Login" name="login" />
					</div>
				</div>
			</form>	
		</div>

		<div class="footer">
			<a href="#">DBQuest::FMF</a>
		</div>

	</body>
</html>

