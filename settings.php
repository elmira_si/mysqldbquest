<?php
session_start();

if (!file_exists('config.php')) {
	$url = 'install/';
	header("Location: $url");
    exit;
}
