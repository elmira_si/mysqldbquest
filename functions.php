<?php

/**
*  
*/
function Leave($url) {
    header("Location: $url");
    exit;
}
/**
*	Encode given text to hash using whirlpool algo
*/
function encode_password($password) {
    return hash('whirlpool', $password);
}
/**
*	Do Login Function
*/
function doLogin($db){
	$showMsg = "";
	if(isset($_POST['login'])){
		$user_login = $_POST['username'];
		$db->bind("user_login",$user_login);
		$user   =  $db->row("SELECT * FROM `".DB_PREFIX."users` WHERE user_login = :user_login");

		if ($user AND ( $user["user_pass"] == encode_password($_POST['password']))){
			$showMsg = false;			
			$_SESSION['user']['login'] = $user_login;
			$_SESSION['user']['role'] = $user['user_role'];
			$_SESSION['user']['email'] = $user['user_email'];
		}else{
			$showMsg = true;
		}	

		return $showMsg;	
	}	
}
/**
*	Do Logout Function
*/
function doLogout(){
	unset($_SESSION['user']);
}
/**
*	Check if user logged in
*/
function isUserLogin(){
	if( isset($_SESSION['user']['login']) && $_SESSION['user']['login'] != ''){
		return true;
	}
	return false;
}
/**
*	Return page url
*/
function thisUrl(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
/**
*	Return Base url
*/
function baseUrl(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'];
}
/**
*	Save Table View Function
*/
function saveTableView($db, $action, $data){   
    
    $selectTables = $data['tables']['selected'];
  	$joinTables = isset($data['tables']['joined']) ? $data['tables']['joined']  : false;
    $conditions = $data['condition'];
    $name =$data['name'];
    $slug=$data['slug'];

    $sql = '';
    $collumnsString = '';
    $selectTableNames = '';
    $joinTableNames = '';
    $joinStr = '';

    $tableViewObj = $data;
    unset($tableViewObj['insert_table_view']);   

    if(isset($selectTables) ){      
      foreach ($selectTables as $tableName => $v) {
        $selectTableNames .= "`".$tableName."`, ";
        //var_dump($v);die();
        foreach ($v as $key => $val ) {
           $collumnsString .= $tableName .'.'. $key.', ';
        }            
      }
      $selectTableNames = rtrim($selectTableNames,", ");        
      if($joinTables){
        foreach ($joinTables as $tableName => $v) {
          $joinOn = $v['joinOn'];
          $joinType = $v['joinType'];
          unset($v['joinOn']);
          unset($v['joinType']);
          $joinStr .= $joinType.' `'.$tableName.'` ON '. $joinOn.' ';
          foreach ($v as $key => $val ) {
             $collumnsString .= $tableName .'.'. $key.', ';
          }            
        }
      }

      $collumnsString = rtrim($collumnsString,", ");

      $sql .= "SELECT ".$collumnsString." FROM ".$selectTableNames .' ';
      $sql .= $joinStr . ' ';

      if($conditions['where'] != ''){
        $sql .= "WHERE ".$conditions['where'].' ';
      }
      if($conditions['groupBy'] != ''){
        $sql .= "GROUP BY ".$conditions['groupBy'].' ';
      }
      if($conditions['orderBy'] != ''){
        $sql .= "ORDER BY ".$conditions['orderBy'].' ';
      }
    }

  	$viewObject = serialize($tableViewObj); //serializing array for save it in db

    //Insert Data to table view
    if($action=='insert'){    	
    	//validate for slug; slug must be unique
  		$getSlug = $db->row("SELECT `view_slug` FROM `".DB_PREFIX."table_views` WHERE view_slug = :view_slug", array("view_slug"=>$data['slug']));
  		if($getSlug['view_slug'] === $slug ){
  			return array(
  						'message'=>'Slug <span>`'.$slug.'`</span> already exists.',
  						'status'=>'error'
  					);
  		}
  		$insert   =  $db->query("INSERT INTO `".DB_PREFIX."table_views`(view_name, view_slug, view_object, view_sql) VALUES(:view_name,:view_slug, :view_object, :view_sql)", array("view_name"=>"$name","view_slug"=>"$slug", "view_object"=>"$viewObject", "view_sql"=>"$sql"));
  		if($insert > 0 ) {
  		  return array(
    						'message'=>'Table View Succesfully created!',
    						'status'=>'success'
    					);
  		} else {
  			return array(
    						'message'=>'Insert Issue',
    						'status'=>'error'
    					);
  		} 

    }


    //Update data into table view    
    if($action=='update'){
        $pageSlug = $_GET['edit'];
        $update = $db->query("UPDATE  `".DB_PREFIX."table_views` SET view_name = :view_name, view_object = :view_object, view_sql = :view_sql WHERE view_slug = :view_slug", array("view_name"=>"$name", "view_object"=>"$viewObject", "view_sql"=>"$sql", "view_slug"=>"$pageSlug"));

        if($update) {
          return array(
                  'message'=>'Table View Succesfully updated!',
                  'status'=>'success'
                );
        } else {
          return array(
                  'message'=>'Update Issue',
                  'status'=>'error'
                );
        }
    } 

}

/**
*  function pagination
*	pagination(
*	   total amount of item/rows/whatever,
*	   limit of items per page,
*	   current page number,
*	   url
*	);
*/
function pagination($item_count, $limit, $cur_page, $link)
{
       $page_count = ceil($item_count/$limit);
       $current_range = array(($cur_page-2 < 1 ? 1 : $cur_page-2), ($cur_page+2 > $page_count ? $page_count : $cur_page+10));

       // First and Last pages
       $first_page = $cur_page > 3 ? '<a href="'.sprintf($link, '1').'">1</a>'.($cur_page < 5 ? ', ' : ' ... ') : null;
       $last_page = ($cur_page < $page_count-2) ? ($cur_page > $page_count-4 ? ', ' : ' ... ').'<a href="'.sprintf($link, $page_count).'">'.$page_count.'</a>' : null;

       // Previous and next page
       $previous_page = $cur_page > 1 ? '<a href="'.sprintf($link, ($cur_page-1)).'">Previous</a> | ' : null;
       $next_page = $cur_page < $page_count ? ' | <a href="'.sprintf($link, ($cur_page+1)).'">Next</a>' : null;

       // Display pages that are in range
       for ($x=$current_range[0];$x <= $current_range[1]; ++$x){       
           $pages[] = '<a href="'.sprintf($link, $x).'">'.($x == $cur_page ? '<strong>'.$x.'</strong>' : $x).'</a>';
       }

       if ($page_count > 1)
               return '<p class="pagination"><strong>Pages:</strong> '.$previous_page.$first_page.implode(', ', $pages).$last_page.$next_page.'</p>';
}