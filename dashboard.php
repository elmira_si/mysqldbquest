<?php  
    require_once('settings.php'); 
    require_once('config.php'); 

    if(!isUserLogin()){
        Leave(SITE_URL);
    }

    if(isset($_GET['logout'])){
        doLogout();
        Leave(SITE_URL);
    }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>DBQuest App Dashboard</title>
		<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
	</head>
	<body>
		<div class="container">
			<ul class="dashboard-nav">				
				<li><a class="active" href="<?php echo SITE_URL; ?>/dashboard.php">Dashboard</a></li>
				<li class="pull-right"><a class="logout" href="<?php echo thisUrl(); ?>/?logout">Log Out</a></li>
			</ul>

			<div class="dashboard">
				<div class="col-50">
					<h2>Table Views <a href="<?php echo SITE_URL; ?>/tableViews.php"" class="pull-right">Show All 	&#8667;</a></h2>
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th>Name</th>
								<th>Slug</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								// Fetch whole table
								$tableViews = $db->query("SELECT * FROM `".DB_PREFIX."table_views` LIMIT 10");
								foreach ($tableViews as $key => $value) {
							?>
							<tr>
								<td><?php echo $value['view_name']; ?></td>
								<td><?php echo $value['view_slug']; ?></td>
								<td class="text-right">
									<a href="table.php?view=<?php echo $value['view_slug']; ?>">View</a> | 
									<a href="tableView.php?edit=<?php echo $value['view_slug']; ?>">Edit</a> | 
									<a href="table.php?delete=<?php echo $value['view_slug']; ?>">Delete</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>					
					</table>

					

				</div>

				<div class="col-50 tools">
					<h2>Tools</h2>
					<p>
						<a class="btn" href="<?php echo SITE_URL; ?>/tableView.php">Create New Table View</a>
					</p>
					<p>
						<a class="btn" href="<?php echo SITE_URL; ?>/tools/templates/template_fmf_tables.xlsx">Download .xlsx template</a>
					</p>
					<p>
						<a class="btn" href="<?php echo SITE_URL; ?>/tools/csv_convert.php" target="_blank">Convert CSV file to DB Table</a>
					</p>

					<h2>Settings </h2>

				</div>
			</div>
			
		</div>

		<div class="footer">
			<a style="color: #000;" href="#">DBQuest::FMF</a>
		</div>

	</body>
</html>