<?php 

$showMsg = doLogin($db);

if(isUserLogin()){
	Leave('dashboard.php');
}

?>


<div class="container">

	<?php if( $showMsg && $showMsg != "" ){ ?>	
		<div class="warning">
		  <strong>Warning!</strong> Username or password is not correct.
		</div>
	<?php } ?>

	<form id="signup" action="" method="post">
		<div class="header">			
			<h3>Sign In</h3>				
			<p>Fill out this form to login the system</p>				
		</div>
		
		<div class="sep"></div>			
			<div class="inputs">				
				<input type="text" placeholder="Username" name="username" autofocus required="required" />	
				<input type="password" placeholder="Password" name="password"  required="required"/>		
				<input id="submit" type="submit" value="Login" name="login" />
			</div>
		</div>
	</form>	
</div>